import 'package:iterable/iterable.dart' as iterable;

void main(List<String> arguments) {
  Iterable<int> iterable = [1, 2, 3];
  int value = iterable.elementAt(1);
  print(value);

  //List
  const foods = ['Salad', 'Popcorn', 'Toast', 'Lasagne'];
  Iterable<String> iterableFoods = foods;
  for (var food in iterableFoods) {
    print(food);
  }

  print('First element is ${foods.first}');
  print('Last element is ${foods.last}');

  var foundItem = foods.firstWhere((element) {
    return element.length > 5;
  });
  print(foundItem);

  var foundItem2 = foods.firstWhere((element) => element.length > 5);
  print(foundItem2);

  bool predicate(String item) {
    return item.length > 5;
  }

  var foundItem3 = foods.firstWhere(predicate);
  print(foundItem3);

  var foundItem4 = foods.firstWhere(
    (item) => item.length > 10,
    orElse: () => 'None!',
  );
  print(foundItem4);

  //ค้นหาตัวที่ขึ้นต้นด้วยอักษรที่กำหนด และมีอักษรที่ต้องการอยู่ในคำนั้นๆ
  var foundItem5 = foods.firstWhere(
      (item) => item.startsWith('M') && item.contains('a'),
      orElse: () => 'None!');
  print(foundItem5);

  //ใช้ if หาค่าตามเงื่อนไข
  if (foods.any((item) => item.contains('a'))) {
    print('At least one item contains "a"');
  }
  if (foods.every((item) => item.length >= 5)) {
    print('All items have length >= 5');
  }

  var numbers = const [1, -2, 3, 42,0, 4, 5, 6];
  var evenNumbers = numbers.where((number) => number.isEven);
  //หาเลขในลิสที่เป็นเลขคู่
  for (var number in evenNumbers) {
    print('$number is even.');
  }
  //เช็คว่าเลขในลิสติดลบไหม
  if (evenNumbers.any((number) => number.isNegative)) {
    print('evenNumbers contains negative numbers.');
  }
  //หาค่าที่มากกว่า 1,000
  var largeNumbers = evenNumbers.where((number) => number > 1000);
  if (largeNumbers.isEmpty) {
    print('largeNumbers is empty!');
  }
  //ไล่วนหาตัวเลขในลิสจนกว่าจะเจอเลข 0
  var numbersUntilZero = numbers.takeWhile((number) => number != 0);
  print('Numbers until 0: $numbersUntilZero');
  //เก็บตัวเลขในลิสที่เริ่มจาก 0 ไปเรื่อยๆ
  var numbersStartingAtZero = numbers.skipWhile((number) => number != 0);
  print('Numbers starting at 0: $numbersStartingAtZero');

  var numbersByTwo = numbers.map((number) => number * 2);
  print('Numbers: $numbersByTwo');
}
